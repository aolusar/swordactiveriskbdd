﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using TechTalk.SpecFlow;

namespace SwordActiveRiskBdd.Steps
{
    [Binding]
    public class FormTestSteps
    {
        private readonly IWebDriver driver;
        private string baseUrl;

        public FormTestSteps(IWebDriver driver)
        {
            this.driver = driver;
            baseUrl = "https://mattsharpe.github.io";
        }

        [Given(@"I browse to the risk form page")]
        public void GivenIBrowseToTheRiskFormPage()
        {
            driver.Navigate().GoToUrl(baseUrl + "/");
        }

        [When(@"I enter the title '(.*)'")]
        public void WhenIEnterTheTitle(string p0)
        {
            driver.FindElement(By.Id("RiskTitle")).Clear();
            driver.FindElement(By.Id("RiskTitle")).SendKeys(p0);
        }

        [When(@"I select the status '(.*)'")]
        public void WhenISelectTheStatus(string p0)
        {
            new SelectElement(driver.FindElement(By.Id("status"))).SelectByText(p0);
        }

        [When(@"I enter the approval date '(.*)'")]
        public void WhenIEnterTheApprovalDate(string p0)
        {
            driver.FindElement(By.Id("approvalDate")).Clear();
            driver.FindElement(By.Id("approvalDate")).SendKeys(p0);
        }

        [When(@"I enter the email '(.*)'")]
        public void WhenIEnterTheEmail(string p0)
        {
            driver.FindElement(By.Id("owner")).Clear();
            driver.FindElement(By.Id("owner")).SendKeys(p0);
        }

        [When(@"I press save")]
        public void WhenIPressSave()
        {
            driver.FindElement(By.CssSelector("button.btn.btn-primary")).Click();
        }

        [Then(@"The form risk should be valid")]
        public void ThenTheFormRiskShouldBeValid()
        {
            var success = driver.FindElement(By.CssSelector("div.alert-success"));
            var sucessClass = success.GetAttribute("class");
            var successIsHidden = sucessClass.Contains("hidden");
            Assert.IsFalse(successIsHidden);
        }

        [Then(@"The form risk should be invalid")]
        public void ThenTheFormRiskShouldBeInvalid()
        {
            var danger = driver.FindElement(By.CssSelector("div.alert-danger"));
            var dangerClass = danger.GetAttribute("class");
            var dangerIsHidden = dangerClass.Contains("hidden");
            Assert.IsFalse(dangerIsHidden);
        }

        [Then(@"The approval date should default to today's date")]
        public void ThenTheApprovalDateShouldDefaultToTodaySDate()
        {
            var date = driver.FindElement(By.Id("approvalDate"));
            var currentValue = date.GetAttribute("value");

            // This date should come in dd/mm/yyyy format, however, it is coming in yyyy-mm-dd
            // We can still parse this date, however, we would advise the front end developer to fix it
            var parsedDate = DateTime.ParseExact(currentValue, "yyyy-M-d", null);
            var today = DateTime.Now;
            Assert.AreEqual(today.Year, parsedDate.Year);
            Assert.AreEqual(today.Month, parsedDate.Month);
            Assert.AreEqual(today.Day, parsedDate.Day);
        }


    }
}
