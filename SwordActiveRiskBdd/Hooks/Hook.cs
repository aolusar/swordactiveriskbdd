﻿using BoDi;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace SwordActiveRiskBdd.Hooks
{
    [Binding]
    public class Hook
    {
        private readonly IObjectContainer objectContainer;

        private IWebDriver driver;

        public Hook(IObjectContainer objectContainer)
        {
            this.objectContainer = objectContainer;
        }

        [BeforeScenario]
        public void Before()
        {
            driver = new FirefoxDriver();
            objectContainer.RegisterInstanceAs(driver);
        }

        [AfterScenario]
        public void AfterScenario()
        {
            try
            {
                driver.Quit();
            }
            catch
            {
                // Ignore errors if unable to close the browser
            }
        }

    }
}
