﻿Feature: FormTest
	In order to demonstrate the correctness of the solution 
	As a tester
	I want to create tests cases that will highlight any errors

	
Scenario: Form success
	Given I browse to the risk form page
	When I enter the title 'Test title'
	And I select the status 'Open'
	And I enter the approval date '03/03/2015'
	And I enter the email 'test@test.com'
	And I press save
	Then The form risk should be valid

Scenario: Form is invalid if the title field is empty
	Given I browse to the risk form page
	When I select the status 'Open'
	And I enter the approval date '03/03/2015'
	And I enter the email 'test@test.com'
	And I press save
	Then The form risk should be invalid

	# This test will fail as the acceptance criteria is that all fields are mandatory the develepor shoul fix this because it is a bug
Scenario: Form is invalid if the email field is empty
	Given I browse to the risk form page
	When I enter the title 'Test title'
	And I select the status 'Open'
	And I enter the approval date '03/03/2015'
	And I press save
	Then The form risk should be invalid

Scenario: Change Status field without specifing approval date
	Given I browse to the risk form page
	When I select the status 'Open'
	And I enter the approval date '03/03/2015'
	And I press save
	And I select the status 'Approved'
	And I press save
	Then The form risk should be invalid


Scenario: Changing status to Approved should default to today's date
Given I browse to the risk form page
	When I enter the title 'Test title'
	And I select the status 'Open'
	And I enter the approval date '03/03/2015'
	And I press save
	And I select the status 'Approved'
	And I press save
	Then The approval date should default to today's date